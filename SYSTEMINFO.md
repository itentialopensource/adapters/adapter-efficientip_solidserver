# Efficient iP SOLIDserver

Vendor: Efficient iP
Homepage: https://efficientip.com/

Product: SOLIDserver
Product Page: https://efficientip.com/products/solidserver-ddi/

## Introduction
We classify Efficient iP SOLIDserver into the CI/CD domain since Efficient iP SOLIDserver provides centralized control and automation for DNS, DHCP, and IP address assignment, enabling organizations to efficiently manage large-scale IP networks. 

## Why Integrate
The Efficient iP SOLIDserver adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Efficient iP SOLIDserver. With this adapter you have the ability to perform operations on:

- Devices
- DHCP
- DNS
- IPAM
- VLAN

## Additional Product Documentation
The [API documents for Efficient iP SOLIDserver](https://efficientip.com/blog/getting-started-with-solidserver-rest-apis/)